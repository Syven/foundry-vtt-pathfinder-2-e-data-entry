{
    "_id": "Upf2fOZ6QgGG3seI",
    "data": {
        "abilities": {
            "cha": {
                "mod": -5
            },
            "con": {
                "mod": 4
            },
            "dex": {
                "mod": -1
            },
            "int": {
                "mod": -5
            },
            "str": {
                "mod": 7
            },
            "wis": {
                "mod": 0
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 30
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 175,
                "temp": 0,
                "value": 175
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 17
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "20"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "blurb": "",
            "creatureType": "Construct",
            "level": {
                "value": 11
            },
            "privateNotes": "",
            "publicNotes": "<p>Stone golems are slow and steady constructs typically carved from marble or granite. They're often made to serve as works of art when at rest, so some golem crafters employ master sculptors to ensure the constructs make beautiful statues. Older stone golems might be weathered, with scuffed or cracked surfaces or missing noses and digits, but this weathering is largely cosmetic and doesn't adversely impact the golems' functionality.</p>\n<p>Tales tell of particularly immense stone golems residing in certain ancient ruins. Survivors from time-lost civilizations bent on carrying out orders from long-gone masters, these immense stone golems are much more powerful than most stone golems. They are always level 15 or higher and never smaller than Huge in size-most are Gargantuan. Because their size is so great and the structures they dwell in so dilapidated, the awakening of such a stone golem can cause surrounding structures to collapse, ancient foundations to buckle, and ceilings to come crashing down on foes. In addition to the statistics here, these massive stone golems attack with wide, sweeping strikes capable of knocking down multiple targets at once.</p>\n<hr />\n<p>Crafted of base materials and then magically animated into a powerful guardian, the legendary golem is a living construct that mindlessly obeys its creator's commands-often continuing to do so for years or even centuries after its creator's death. There exist two known methods of animating a golem. The traditional method involves harvesting and implanting an elemental soul or essence within the newly crafted host statue, a procedure seen as vile and blasphemous to those who value the sanctity of the soul; evil or amoral golem crafters tend to prefer this method. The other, less disreputable technique involves siphoning pure positive energy into the statue to artificially imitate the creation of a soul. The result does not give the golem a true soul and is generally a more costly and time-consuming method of creation. Regardless of the method used, the resulting golem functions the same. A golem's unique animating force leaves it susceptible to certain forms of magic, but apart from these few weaknesses, it is impervious to magic and difficult to damage with weapons.</p>\n<p>Golems work best in play as foes to vanquish rather than allies to accompany player characters on adventures. The process of creating a golem is time-consuming, expensive, and difficult, and only the most talented spellcasters or artisans can even hope to accomplish such an undertaking. While certain magical texts-so-called \"golem manuals\"-are said to aid golems crafters, for the most part the creation of a golem should be something left in the hands of the Game Master.</p>\n<p>Golems have components that can be harvested as trophies or magical components; the value depends on the golem in question. Examples of components that can be harvested from golems are listed in the sidebars.</p>",
            "source": {
                "author": "",
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 24
            },
            "reflex": {
                "saveDetail": "",
                "value": 18
            },
            "will": {
                "saveDetail": "",
                "value": 19
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "Magic (see Golem Antimagic)",
                "value": [
                    "acid",
                    "bleed",
                    "death-effects",
                    "disease",
                    "doomed",
                    "drained",
                    "fatigued",
                    "healing",
                    "mental",
                    "necromancy",
                    "nonlethal-attacks",
                    "paralyzed",
                    "poison",
                    "sickened",
                    "unconscious"
                ]
            },
            "dr": [
                {
                    "exceptions": "adamantine",
                    "type": "physical",
                    "value": 10
                }
            ],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": []
            },
            "rarity": {
                "value": "uncommon"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "lg"
            },
            "traits": {
                "custom": "",
                "value": [
                    "construct",
                    "golem",
                    "mindless"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "B9YtjVsCCmeVw39y",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": []
                },
                "bonus": {
                    "value": 24
                },
                "damageRolls": {
                    "hkdjhqfm65kr1ha9mhks": {
                        "damage": "2d10+13",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "magical",
                        "reach-10"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Fist",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "cYDXg8exhcvpvPs8",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.Darkvision]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "darkvision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6"
                }
            },
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Darkvision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "eYD6EiiIYvny4Vq2",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>harmed by cold and water ([[/r {5d10}]]{5d10 damage}, [[/r {2d8}]]{2d8 damage} from areas or persistent damage); healed by acid (area [[/r {2d8}[healing]]]{2d8 Hit Points}); slowed by earth</p>\n<hr />\n<p>A golem is immune to spells and magical abilities other than its own, but each type of golem is affected by a few types of magic in special ways. These exceptions are listed in shortened form in the golem's stat block, with the full rules appearing here. If an entry lists multiple types (such as \"cold and water\"), either type of spell can affect the golem.</p>\n<ul>\n<li><strong>Harmed By</strong> Any magic of this type that targets the golem causes it to take the listed amount of damage (this damage has no type) instead of the usual effect. If the golem starts its turn in an area of magic of this type or is affected by a persistent effect of the appropriate type, it takes the damage listed in the parenthetical.</li>\n<li><strong>Healed By</strong> Any magic of this type that targets the golem makes the golem lose the slowed condition and gain HP equal to half the damage the spell would have dealt. If the golem starts its turn in an area of this type of magic, it gains the HP listed in the parenthetical.</li>\n<li><strong>Slowed By</strong> Any magic of this type that targets the golem causes it to be @Compendium[pf2e.conditionitems.Slowed]{Slowed 1} for [[/br 2d6 #rounds]]{2d6 rounds} instead of the usual effect. If the golem starts its turn in an area of this type of magic, it's slowed 1 for that round.</li>\n<li><strong>Vulnerable To</strong> Each golem is vulnerable to one or more specific spells, with the effects described in its stat block.</li>\n</ul>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "golem-golem-antimagic",
                "source": {
                    "value": "Pathfinder Bestiary"
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-family-ability-glossary.r34QDwKiWZoVymJa"
                }
            },
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Golem Antimagic",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "Jg5WkDHWpi1JRQ9k",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A <em>@Compendium[pf2e.spells-srd.Stone to Flesh]{Stone to Flesh}</em> spell negates the golem's golem antimagic and its resistance to physical damage for 1 round. A <em>@Compendium[pf2e.spells-srd.Flesh to Stone]{Flesh to Stone}</em> spell reverses this effect immediately.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Vulnerable to Stone to Flesh",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "RlajqjwuSiKB8NGC",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p data-visibility=\"gm\"><strong>Trigger</strong> The stone golem hits a @Compendium[pf2e.conditionitems.Slowed]{Slowed} creature.</p>\n<p><strong>Effect</strong> The creature must succeed at a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"30\" data-pf2-traits=\"incapacitation\" data-pf2-label=\"Impose Paralysis DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save or become @Compendium[pf2e.conditionitems.Paralyzed]{Paralyzed} for 1 round.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "incapacitation"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Reaction.webp",
            "name": "Impose Paralysis",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "bKmTRJI4PjS70sWZ",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p><span data-pf2-check=\"fortitude\" data-pf2-dc=\"34\" data-pf2-traits=\"damaging-effect\" data-pf2-label=\"Inexorable March DC\" data-pf2-show-dc=\"gm\">Fortitude</span></p>\n<p data-visibility=\"gm\">On a critical success, the resisting creature takes no damage; otherwise it is damaged as if hit by the golem's fist.</p>\n<hr />\n<p>The golem Strides up to its Speed, pushing back each creature whose space it moves into and damaging them if they try to stop its movement. A creature can attempt to bar the way by succeeding at a Fortitude save.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "golem-inexorable-march",
                "source": {
                    "value": "Pathfinder Bestiary"
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-family-ability-glossary.qt2exWwQTzoObKfW"
                }
            },
            "img": "systems/pf2e/icons/actions/OneAction.webp",
            "name": "Inexorable March",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "11mRPoFSEt1nYAnE",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>Each creature in a @Template[type:emanation|distance:10]{10-foot emanation} must succeed at a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"30\" data-pf2-traits=\"arcane,concentrate,transmutation\" data-pf2-label=\"Slowing Pulse DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save or be @Compendium[pf2e.conditionitems.Slowed]{Slowed 1} for 1 minute.</p>\n<p data-visibility=\"gm\">The golem can't use Slowing Pulse again for [[/br 1d4 #Slowing Pulse Recharge]]{1d4 rounds}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "arcane",
                        "concentrate",
                        "transmutation"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/OneAction.webp",
            "name": "Slowing Pulse",
            "sort": 700000,
            "type": "action"
        },
        {
            "_id": "wxWN3ntgIKwYkj68",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 26
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 800000,
            "type": "lore"
        }
    ],
    "name": "Stone Golem",
    "token": {
        "disposition": -1,
        "height": 2,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Stone Golem",
        "width": 2
    },
    "type": "npc"
}
